package tdaf.kafka.kafkachat.storage;

import org.springframework.stereotype.Component;
import tdaf.kafka.kafkachat.model.Message;

import java.util.*;

@Component
public class ChatStorage {
    private Map<Long, Message> chatMessages = new LinkedHashMap<>();

    public void put(Message message){

        chatMessages.put(message.getId(), message);
    }

    public List<Long> getMessageIds(){
        List<Long> ids = new ArrayList<>();
        chatMessages.forEach((id, msg) -> ids.add(id));
        return ids;
    }

    public Message getMessage(Long id){
        return chatMessages.get(id);
    }

    public String getMessages(){
        StringBuffer info = new StringBuffer();

        chatMessages.forEach((id, msg) -> info.append(id).
                append(" > ").
                append(msg.getUser()).
                append(" : ").
                append(msg.getMessage()).
                append("<br/>"));

        return info.toString();
    }
}
