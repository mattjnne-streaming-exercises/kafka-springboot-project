package tdaf.kafka.kafkachat.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import tdaf.kafka.kafkachat.model.Message;
import tdaf.kafka.kafkachat.services.KafkaProducer;
import tdaf.kafka.kafkachat.storage.ChatStorage;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value="/tdaf/kafka")
public class WebRestController {
    private static final Logger log = LoggerFactory.getLogger(WebRestController.class);

    @Autowired
    KafkaProducer producer;

    @Autowired
    ChatStorage chat;

    // Send chat message
    @PostMapping(path="/chats", consumes = "application/json", produces = "application/json")
    public @ResponseBody ResponseEntity<String> postMsg(@RequestBody Message message){
        producer.send(message);
        return new ResponseEntity<>("Post sent", HttpStatus.OK);
    }

    @GetMapping(value="/chats")
    public @ResponseBody List<Message> getChatMessages(){
        List<Long> chatIds = chat.getMessageIds();
        List<Message> msgs = new ArrayList<>();

        if (chatIds != null || chatIds.size() > 0) {
            chatIds.forEach((id) -> msgs.add(chat.getMessage(id)));
        }
        return msgs;
    }

    @GetMapping(value="/parsed")
    public String getAllRecievedMessage(){
        return chat.getMessages();
    }
}