package tdaf.kafka.kafkachat.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import tdaf.kafka.kafkachat.model.Message;
import tdaf.kafka.kafkachat.storage.ChatStorage;

@Component
public class KafkaConsumer {
    private static final Logger log = LoggerFactory.getLogger(KafkaProducer.class);

    @Autowired
    ChatStorage storage;

    @KafkaListener(topics="${tdaf.kafka.topic}")
    public void processMessage(Message rcvdMsg) {
        log.info("received content = '{}'", rcvdMsg);
        storage.put(rcvdMsg);
    }
}