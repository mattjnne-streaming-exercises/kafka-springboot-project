package tdaf.kafka.kafkachat.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import tdaf.kafka.kafkachat.model.Message;

import java.time.Instant;

@Service
public class KafkaProducer {
    private static final Logger log = LoggerFactory.getLogger(KafkaProducer.class);

    @Autowired
    private KafkaTemplate<String, Message> kafkaTemplate;

    @Value("${tdaf.kafka.topic}")
    String kafkaTopic;

    public void send(Message message) {
        long now = Instant.now().toEpochMilli();
        message.setId(now);
        log.info("sending message='{}'", message.toString());
        kafkaTemplate.send(kafkaTopic, message.getId().toString(), message);
    }
}