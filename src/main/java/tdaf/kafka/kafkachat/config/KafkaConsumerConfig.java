package tdaf.kafka.kafkachat.config;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import tdaf.kafka.kafkachat.model.ChatMessageSerializer;
import tdaf.kafka.kafkachat.model.Message;

@EnableKafka
@Configuration
public class KafkaConsumerConfig {

    @Value("${tdaf.kafka.bootstrap-servers}")
    private String bootstrapServer;

    @Value("${tdaf.kafka.consumer.client-id}")
    private String clientId;

    /*
      TODO Usually you want to use fixed groupId
      For this exercise the groupId will change every time.
      See detailed explanation in the next comment block.
     */
    //@Value("${tdaf.kafka.consumer.group-id}")
    //private String groupId;
    private String groupId = UUID.randomUUID().toString();

    @Bean
    public ConsumerFactory<String, Message> consumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, clientId);
        /*
          TODO Set the behavior how messages are read from the topic.
          Kafka uses groupId information to store the offset. When consumer with new groupId is requesting messages
          Kafka will use offset based on AUTO_OFFSET_RESET_CONFIG.
          String must be one of:
          earliest: automatically reset the offset to the earliest offset -> Start from the beginning
          latest: automatically reset the offset to the latest offset -> Start from the end
          none: throw exception to the consumer if no previous offset is found or the consumer's group -> Start where left previously
         */
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ChatMessageSerializer.class);
        return new DefaultKafkaConsumerFactory<>(props);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, Message> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, Message> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }
}