package tdaf.kafka.kafkachat.model;

public class Message {
    private Long id;
    private String user;
    private String message;

    public Message(){}

    public Message(Long id, String user, String message) {
        this.id = id;
        this.user = user;
        this.message = message;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
