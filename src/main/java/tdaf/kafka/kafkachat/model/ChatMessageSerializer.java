package tdaf.kafka.kafkachat.model;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Arrays;
import java.util.Locale;
import java.util.Map;

public class ChatMessageSerializer implements Serializer<Message>, Deserializer<Message> {

    @Override
    public Message deserialize(String s, byte[] line) {

        //return Message.fromString(new String(message));
        String[] tokens = new String(line).split(";");
        if (tokens.length != 3) {
            throw new RuntimeException("Invalid record: " + Arrays.toString(line));
        }

        Message lineMessage = new Message();

        lineMessage.setId(Long.parseLong(tokens[0]));
        lineMessage.setUser(tokens[1]);
        lineMessage.setMessage(tokens[2]);

        return lineMessage;
    }

    @Override
    public void configure(Map<String, ?> map, boolean b) {

    }

    @Override
    public byte[] serialize(String s, Message element) {
        String line = String.format(Locale.ROOT, "%s;%s;%s",
                element.getId(),
                element.getUser(),
                element.getMessage());
        return line.getBytes();
    }

    @Override
    public void close() {

    }
}
