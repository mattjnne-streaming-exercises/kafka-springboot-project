# TDAF STREAMING KAFKA CHAT EXERCISE
In this exercise we will use small Spring-boot application to create simple chat.
Spring boot will provide REST API to post chat messages and read chat log.

![Overview](Exer2-overview.png)

## Prerequisites
*Java JDK 8*

*Kafka + zookeeper needs* to be running somewhere. Local setup is easy put
up by following Kafka guide https://kafka.apache.org/quickstart.
Topic can be created but not necessary depending on your Kafka configuration - is
topic auto creation allowed?

For the TAA Session 14 the Kafka is setup for you to AWS.

Zookeeper hosts:
```
ec2-52-49-177-18.eu-west-1.compute.amazonaws.com:2181,
ec2-34-248-141-141.eu-west-1.compute.amazonaws.com:2181,
ec2-52-214-15-180.eu-west-1.compute.amazonaws.com:2181
```
Kafka hosts (bootstrap-servers):
```
ec2-52-49-177-18.eu-west-1.compute.amazonaws.com:9092,
ec2-34-248-141-141.eu-west-1.compute.amazonaws.com:9092,
ec2-52-214-15-180.eu-west-1.compute.amazonaws.com:9092
```
Topic
```
chat
```

## Code key points
### Producer
*KafkaProducerConfig* creates ProducerFactory that is used to build the KafkaTemplate.
ProducerFactory contains the basic properties needed to create connection
into Kafka broker. The important parts here are the address of the initial
server (bootstrap.servers) and serializers for key and value.

*KafkaProducer* simply handles the message sending to a configured topic using KafkaTemplate.


### Consumer
*KafkaConsumerConfig* creates ConsumerFactory that is used to build the
ConcurrentKafkaListenerContainerFactory which will handle `@KafkaListener`.
ConsumerFactory contains the basic properties needed to create connection
into Kafka broker and its detailed characteristics like offset handling.

Notice that `@EnableKafka` is used to enable detection of `@KafkaListener` annotation.

*KafkaConsumer* has a method for handling received messages. This is annotated
with `@KafkaListerner`. In this example the messages are stored into ChatStorage.

### Common
*ChatMessageSerializer* implements Kafkas serializer and deserializer. These
are used to serialize message into Kafka topic and to deserialize them when
read from topic.
Kafka has several default serializers. This exercise uses one of them.

## Running the code
To get the application run compile it and run on your laptop. Mac / Linux
users, check that you have needed permissions to run mvnw command.
#### Edit properties
Open `resources/application.properties`. Edit _bootstrap-servers_ and
_zookeeper-connect_ values with ones provided to you. 
Also edit the details for _group-id_ and _client-id_.

#### Compile
`mvnw clean install -Dmaven.test.skip=true`

#### Run application
`mvnw spring-boot:run`

Once started is should look like this.
```
...
2017-12-04 22:12:43.577  INFO 6388 --- [           main] o.a.kafka.common.utils.AppInfoParser     : Kafka version : 0.10.1.1
2017-12-04 22:12:43.579  INFO 6388 --- [           main] o.a.kafka.common.utils.AppInfoParser     : Kafka commitId : f10ef2720b03b247
2017-12-04 22:12:43.920  INFO 6388 --- [           main] s.b.c.e.t.TomcatEmbeddedServletContainer : Tomcat started on port(s): 9090 (http)
2017-12-04 22:12:43.933  INFO 6388 --- [           main] t.kafka.kafkachat.KafkaChatApplication   : Started KafkaChatApplication in 11.788 seconds (JVM running for 25.452)
```

When application has started you can open your browser and go to
http://ip-address-given-to-you:9090/tdaf/kafka/chats

POST request with JSON body described below will post new messages to the chat.
```
{
  "user": "TAA participant",
  "message": "I love Kafka!"
}
```

Here is the same as a curl command.
`curl -H "Content-Type: application/json" -X POST -d '{"user":"TAA participant","message":"I love Kafka!"}' http://ip-address-given-to-you:9090/tdaf/kafka/chats`

Windows users use Git Bash or Docker Quickstart Terminal as your terminal for curl command.